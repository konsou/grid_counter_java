package com.konso.grid_counter_java;

/** Represents a single pixel */
public class Coordinate{
    private int x;
    private int y;
    private int value;
    private Area area = null;

    public Coordinate(int x, int y, int value){
        this.x = x;
        this.y = y;
        this.value = value;
    }

    public String toString(){
        return "<" + Coordinate.class.getSimpleName() + " x: " + x + ", y: " + y + ", value: " + value + ">";
    }

    public String toShortString(){
        return "(" + x + ", " + y + ")";
    }

    public Area getArea() {
        return area;
    }

    public void addToArea(Area area) {
        this.area = area;
        area.addCoordinate(this);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}

