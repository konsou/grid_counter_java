package com.konso.grid_counter_java;

import static java.lang.System.currentTimeMillis;

public class Timer{
    private long start_time;
    private String infoText = "";

    public Timer(String infoText){
        this.infoText = infoText;
    }

    public Timer(){
        this("");
    }

    public void start(String infoText){
        this.infoText = infoText;
        start_time = currentTimeMillis();
    }

    public void start(){
        this.start("");
    }

    public long stop(){
        return currentTimeMillis() - start_time;
    }

    public long stop_and_print(){
        long runtime = currentTimeMillis() - start_time;
        System.out.println(infoText + " runtime: " + runtime + " ms");
        return runtime;
    }
}
