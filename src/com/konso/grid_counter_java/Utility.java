package com.konso.grid_counter_java;

public class Utility {
    /** There's probably a built-in way to do this but who cares */
    public static double average(long[] array){
        double total = 0;
        for (long number: array){
            total += number;
        }
        return total / array.length;
    }

    public static String arrayToString(long[] array){
        String retStr = "";
        boolean first = true;
        for (long number: array){
            if (first){ retStr += number; first = false; }
            else { retStr += ", " + number; }

        }
        return retStr;
    }

}
