package com.konso.grid_counter_java;

import javafx.event.ActionEvent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;

public class Controller {
    public Button loadButton;
    public Button findButton;
    public Label resultsLabel;
    private String imageFileName;
    private Image image;
    private FileChooser fileChooser;
    private Stage stage;
    public ImageView imageView;

    public void setStage(Stage stage){
        System.out.println("Stage set: " + stage.toString());
        this.stage = stage;
    }

    public void loadImage(){
        System.out.println("loadImage called");
        try {
            fileChooser = new FileChooser();
            fileChooser.setTitle("Avaa kuva");
            File file = fileChooser.showOpenDialog(stage);
            if (file != null) {
                imageFileName = file.getAbsolutePath();
                image = new Image(new FileInputStream(file));
                imageView.setImage(image);
                findButton.setDisable(false);
                System.out.println("Loaded image");
            }
        }
        catch (IOError|FileNotFoundException e) {
            System.out.println("Error" + e.toString());
        }

    }

    public void clickFindBiggestArea(ActionEvent actionEvent) {
        if (imageView.getImage() != null) {
            System.out.println("Finding biggest area");
            try {
                Area biggest = GridCounter.findBiggestArea(imageFileName);
                resultsLabel.setText(biggest.toString());
            }
            catch (IOException e){
                resultsLabel.setText("Error " + e.toString());
            }

        }
        else {
            System.out.println("No image!");
        }
    }
}
