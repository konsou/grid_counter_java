package com.konso.grid_counter_java;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;

public class GridCounter {
    /** The default method for finding the biggest Area - no extra debug info */
    public static Area findBiggestArea(String imageFileName) throws IOException {
        // Grid contains image pixel data
        int[][] grid;
        // CoordinateGrid contains Coordinate objects that have been inited from pixel data
        Coordinate[][] coordinateGrid;
        List<Area> areas;
        Area biggestArea;
        ////////////////////

        // Load image as B/W
        grid = loadImage(imageFileName);

        // Convert image pixel data to Coordinate objects
        coordinateGrid = convertGrid(grid);

        // Create initial Areas
        areas = createAreas(coordinateGrid);

        // Merge touching Areas
        areas = mergeAreas(areas);

        // Tursa: if the program continuously keeps track of Areas then this is the only line needed
        // Sort Areas by size (that is, number of Coordinate objects they contain)
        areas.sort(Comparator.comparingInt(Area::getSize));
        // Last area is the biggest (sort is ascending)
        biggestArea = areas.get(areas.size() - 1);

        return biggestArea;
    }

    /** Debug overload - when called with DEBUG but no LOOPS value - default to 100 LOOPS */
    public static Area findBiggestArea(String imageFileName, boolean DEBUG) throws IOException{
        return findBiggestArea(imageFileName, DEBUG, 100);
    }

    /** Debug overload - run find process LOOPS times and print nice timing info if DEBUG is true */
    public static Area findBiggestArea(String imageFileName, boolean DEBUG, int LOOPS) throws IOException{
        // If DEBUG is false -> run normally
        if (!DEBUG) { return findBiggestArea(imageFileName); }

        ///////////////////
        // ACTUAL THINGS
        // Grid contains image pixel data
        int[][] grid;
        // CoordinateGrid contains Coordinate objects that have been inited from pixel data
        Coordinate[][] coordinateGrid;
        List<Area> areas;
        Area biggestArea = null;
        ////////////////////

        ////////////////////
        // Debug stuff
        // final int LOOPS;
        long[] convertTimes;
        long[] createTimes;
        long[] mergeTimes;
        long[] sortTimes;
        long[] totalTimes;
        Timer timer;
        Timer totalTimer;

        convertTimes = new long[LOOPS];
        createTimes = new long[LOOPS];
        mergeTimes = new long[LOOPS];
        sortTimes = new long[LOOPS];
        totalTimes = new long[LOOPS];
        // Debug stuff end
        ////////////////////


        ////////////////////
        // Actual things happening
        // Load image as B/W
        grid = loadImage(imageFileName);
        ////////////////////


        System.out.println(LOOPS + " LOOPS");
        System.out.print("Running");
        for (int counter=0; counter<LOOPS; counter++) {
            if (counter % 10 == 0) {
                System.out.print(".");
            }
            timer = new Timer();
            totalTimer = new Timer();

            totalTimer.start("Total");
            timer.start("convertGrid");

            ////////////////////
            // Actual things happening
            // Convert image pixel data to Coordinate objects
            coordinateGrid = convertGrid(grid);
            ////////////////////


            convertTimes[counter] = timer.stop();
            timer.start("createAreas");

            ////////////////////
            // Actual things happening
            // Create initial Areas
            areas = createAreas(coordinateGrid);
            ////////////////////

            createTimes[counter] = timer.stop();
            timer.start("mergeAreas");

            ////////////////////
            // Actual things happening
            // Merge touching Areas
            areas = mergeAreas(areas);
            ////////////////////

            mergeTimes[counter] = timer.stop();
            timer.start("sort");

            ////////////////////
            // Actual things happening
            // Tursa: if the program continuously keeps track of Areas then this is the only line needed
            // Sort Areas by size (that is, number of Coordinate objects they contain)
            areas.sort(Comparator.comparingInt(Area::getSize));
            // Last area is the biggest (sort is ascending)
            biggestArea = areas.get(areas.size() - 1);
            ////////////////////

            sortTimes[counter] = timer.stop();
            totalTimes[counter] = totalTimer.stop();
        }

        // Nice timing info
        System.out.println();
        System.out.println("Biggest Area: " + biggestArea);
        System.out.println("convertGrid average / all runtimes (ms):  " + Double.toString(Utility.average(convertTimes)) + " - " + Utility.arrayToString(convertTimes));
        System.out.println("createAreas average / all runtimes (ms):  " + Double.toString(Utility.average(createTimes)) + " - " + Utility.arrayToString(createTimes));
        System.out.println("mergeAreas  average / all runtimes (ms):  " + Double.toString(Utility.average(mergeTimes)) + " - " + Utility.arrayToString(mergeTimes));
        System.out.println("sort        average / all runtimes (ms):   " + Double.toString(Utility.average(sortTimes)) + " - " + Utility.arrayToString(sortTimes));
        System.out.println("Total       average / all runtimes (ms): " + Double.toString(Utility.average(totalTimes)) + " - " + Utility.arrayToString(totalTimes));

        return biggestArea;
    }


    /** main can be called with a filename command line argument */
    public static void main(String[] args) {
        // Default filename if no command line argument
        String filename = "";
        // = "C:\\bitbucket\\grid_counter_java\\src\\com\\konso\\grid_counter_java\\pictures\\bw-area-test-image.bmp";
        String commandLineSwitch;
        boolean verbose = false;

        try {
            filename = args[0];
        }
        catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Usage:");
            System.out.println();
            System.out.println("java -jar GridCounter.jar \"image filename.bmp\"");
            System.out.println("    * finds the biggest area from given image file");
            System.out.println();
            System.out.println("java -jar GridCounter.jar \"image filename.bmp\" /v");
            System.out.println("    * /v = verbose mode");
            System.exit(-1);
        }

        try {
            commandLineSwitch = args[1];
        }
        catch (ArrayIndexOutOfBoundsException e){
            commandLineSwitch = "";
        }

        if (commandLineSwitch.toLowerCase().equals("/v") || commandLineSwitch.toLowerCase().equals("/verbose")){
            verbose = true;
        }

        System.out.println("\n\nReading file " + filename);

        try {
            Area area = findBiggestArea(filename);
            System.out.println("\nBiggest Area:");
            if (verbose) {
                System.out.println(area.fullInfoString());
            }
            else {
                System.out.println(area);
            }
        }
        catch(IOException e){
            System.out.println(e.getMessage() + " (" + filename + ")");
        }
    }

    /** Create initial areas column by column.
     *  Add touching info if area touches another area.
     *  Touching info will be used to merge areas later. */
    private static List<Area> createAreas(Coordinate[][] coordinateGrid){
        List<Area> returnList = new ArrayList<>();
        Coordinate previous_cell, cell_to_the_left;
        Area newArea;

        for (Coordinate[] column: coordinateGrid){
            for (Coordinate cell: column){

                if (cell.getValue() == 1){
                    if (cell.getY() > 0){ previous_cell = coordinateGrid[cell.getX()][cell.getY()-1]; }
                    else { previous_cell = null; }

                    if (cell.getX() > 0){ cell_to_the_left = coordinateGrid[cell.getX()-1][cell.getY()]; }
                    else { cell_to_the_left = null; }


                    // Jos yläpuolella on solu joka on 1 ja jolla on jo alue niin otetaan sama alue
                    if (previous_cell != null && previous_cell.getArea() != null){
                        cell.addToArea(previous_cell.getArea());
                    }
                    // Sama juttu vasemmalla puolella olevalle solulle
                    else if (cell_to_the_left != null && cell_to_the_left.getArea() != null){
                        cell.addToArea(cell_to_the_left.getArea());
                    }
                    // Muussa tapauksessa luodaan uusi alue
                    else {
                        newArea = new Area();
                        cell.addToArea(newArea);
                        // Lisätään aluelistaan uusi alue
                        returnList.add(newArea);
                    }


                    // Jos vasemmalla puolella on solu jolla on alue niin lisätään kosketustiedot
                    if (cell_to_the_left != null && cell_to_the_left.getArea() != null &&
                            cell_to_the_left.getArea() != cell.getArea()){
                        cell.getArea().addTouches(cell_to_the_left.getArea());
                        cell_to_the_left.getArea().addTouches(cell.getArea());
                    }
                }
            }
        }
        return returnList;
    }

    /** Merge Areas based on touching info */
    private static List<Area> mergeAreas(List<Area> areas){
        List<Area> returnList = new ArrayList<>();
        Area newArea, currentArea;
        Set<Area> areasLeftToHandle;
        List<Area> touchingAreas = new ArrayList<>();

        for(Area area: areas){
            if (!area.isMerged()){
                newArea = new Area();
                touchingAreas.clear();

                areasLeftToHandle = area.getTouches();

                while (areasLeftToHandle.size() > 0){
                    currentArea = areasLeftToHandle.iterator().next();
                    areasLeftToHandle.remove(currentArea);

                    touchingAreas.add(currentArea);
                    currentArea.setMerged(true);

                    for (Area tchArea: currentArea.getTouches()){
                        if (!tchArea.isMerged()){
                            areasLeftToHandle.add(tchArea);
                        }
                    }
                }

                for (Area tchArea: touchingAreas){
                    newArea.addCoordinates(tchArea.getCoordinates());
                }

                returnList.add(newArea);
            }
        }

        return returnList;
    }

    /**  Load an image and convert its pixels to 1/0  */
    private static int[][] loadImage(String filename) throws IOException{
        BufferedImage img = ImageIO.read(new File(filename));
        int[][] return_array = new int[img.getWidth()][img.getHeight()];

        int value;

        for (int y=0; y<img.getHeight(); y++){
            for (int x=0; x<img.getWidth(); x++){
                if (img.getRGB(x, y) == -1){ value = 0; }
                else { value = 1; }
                return_array[x][y] = value;
            }
        }

        return return_array;
    }

    /** Convert grid (array of ints) to an array grid of Coordinates */
    private static Coordinate[][] convertGrid(int[][] grid){
        Coordinate[][] return_array = new Coordinate[grid.length][grid.length];

        for (int x=0; x < grid.length; x++){
            for (int y=0; y < grid[0].length; y++){
                //System.out.println(x + " " + y);
                return_array[x][y] = new Coordinate(x, y, grid[x][y]);
            }
        }
        return return_array;
    }
}
