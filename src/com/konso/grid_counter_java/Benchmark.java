package com.konso.grid_counter_java;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Benchmark {
    public static void main(String[] args){
        final int NUM = 1000;
        final int LOOPS = 250;
        final int OUTER_LOOPS = 5;
        long trash = 0;

        Timer timer = new Timer();

        long[] arrayForeachResults = new long[OUTER_LOOPS];
        long[] arrayForResults = new long[OUTER_LOOPS];
        long[] arrayListResults = new long[OUTER_LOOPS];
        long[] linkedListResults = new long[OUTER_LOOPS];

        // Init arrays and Lists
        System.out.println("Init start...");
        int[][] arrayForeach = new int[NUM][NUM];
        int[][] arrayFor = new int[NUM][NUM];
        List<List<Integer>> arrayList = new ArrayList<>();
        List<List<Integer>> linkedList = new LinkedList<>();

        for (int i = 0; i < NUM; i++){
            List<Integer> newColumnArrayList = new ArrayList<>();
            List<Integer> newColumnLinkedList = new LinkedList<>();
            for (int j = 0; j < NUM; j++){
                newColumnArrayList.add(0);
                newColumnLinkedList.add(0);
            }
            arrayList.add(newColumnArrayList);
            linkedList.add(newColumnLinkedList);
        }
        System.out.println("Init ready.");

        for (int outerCounter = 0; outerCounter < OUTER_LOOPS; outerCounter++) {
            timer.start(String.format("Array (%sx%s) for loop iteration - %s times", NUM, NUM, LOOPS));
            trash = 0;
            for (int counter = 0; counter < LOOPS; counter++) {
                // Loop using for
                for (int x = 0; x < NUM; x++) {
                    for (int y = 0; y < NUM; y++) {
                        trash += arrayFor[x][y];
                        if (x > 0){ trash += arrayFor[x-1][y]; }
                        if (y > 0){ trash += arrayFor[x][y-1]; }
                    }
                }
            }
            arrayForResults[outerCounter] = timer.stop();

            timer.start(String.format("Array (%sx%s) foreach iteration - %s times", NUM, NUM, LOOPS));
            trash = 0;
            for (int counter = 0; counter < LOOPS; counter++) {
                int x = 0;
                // Loop using foreach
                for (int[] column : arrayForeach) {
                    int y = 0;
                    for (int i : column) {
                        trash += i;
                        if (x > 0){ trash += arrayForeach[x-1][y]; }
                        if (y > 0){ trash += arrayForeach[x][y-1]; }
                        y++;
                    }
                    x++;
                }
            }
            arrayForeachResults[outerCounter] = timer.stop();

            timer.start(String.format("ArrayList (%sx%s) for loop iteration - %s times", NUM, NUM, LOOPS));
            trash = 0;
            for (int counter = 0; counter < LOOPS; counter++) {
                int x = 0;
                // Loop ArrayList
                // List<Integer> previousColumn = null;
                for (List<Integer> column: arrayList) {
                    int y = 0;
                    // Integer previousCell = null;
                    for (int value: column) {
                        trash += value;
                        if (x > 0){ trash += arrayList.get(x-1).get(y); }
                        if (y > 0){ trash += arrayList.get(x).get(y-1); }
                        y++;
                        // previousCell = value;
                    }
                    x++;
                    // previousColumn = column;
                }
            }
            arrayListResults[outerCounter] = timer.stop_and_print();

            timer.start(String.format("LinkedList (%sx%s) for loop iteration - %s times", NUM, NUM, LOOPS));
            trash = 0;
            System.out.print("LinkedList ");
            for (int counter = 0; counter < LOOPS; counter++) {
                if (counter % 50 == 0){ System.out.print(counter + " "); }

                int x = 0;
                // Loop LinkedList
                List<Integer> previousColumn = null;
                for (List<Integer> column: linkedList) {
                    int y = 0;
                    Integer previousCell = null;
                    for (int value: column) {

                        trash += value;
                        if (previousCell != null) { trash += previousCell; }
                        if (previousColumn != null) { trash += previousColumn.get(y); }
                        y++;
                        previousCell = value;
                    }
                    x++;
                    previousColumn = column;
                }
            }
            linkedListResults[outerCounter] = timer.stop_and_print();

        }
        System.out.println(String.format("Array -           for: average %s, all: %s", Utility.average(arrayForResults), Utility.arrayToString(arrayForResults)));
        System.out.println(String.format("Array -       foreach: average %s, all: %s", Utility.average(arrayForeachResults), Utility.arrayToString(arrayForeachResults)));
        System.out.println(String.format("ArrayList -   foreach: average %s, all: %s", Utility.average(arrayListResults), Utility.arrayToString(arrayListResults)));
        System.out.println(String.format("LinkedList -  foreach: average %s, all: %s", Utility.average(linkedListResults), Utility.arrayToString(linkedListResults)));

    }
}
