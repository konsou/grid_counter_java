package com.konso.grid_counter_java;

import java.util.*;

/** An area containing Coordinates */
public class Area{
    private static int idCounter = 0;
    private int id;
    private boolean merged = false;
    private Set<Area> touches = new HashSet<>();
    private List<Coordinate> coordinates = new ArrayList<>();

    public boolean isMerged() {
        return merged;
    }

    public void setMerged(boolean merged) {
        this.merged = merged;
    }

    public Set<Area> getTouches() {
        return touches;
    }

    public void addTouches(Area area) {
        this.touches.add(area);
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public void addCoordinate(Coordinate coord) {
        this.coordinates.add(coord);
    }

    public void addCoordinates(List<Coordinate> coordList) {
        for (Coordinate coordinate: coordList){
            addCoordinate(coordinate);
        }
    }

    public Area(){
        id = idCounter;
        idCounter++;
    }

    public int getSize(){ return coordinates.size(); }

    public String toString(){
        return "<Area " + id + " at " + coordinates.get(0).toShortString() + ", contains " + coordinates.size() + " coordinates>"; //, touches " + touches.size() + " Areas>";
    }

    public String fullInfoString(){
        StringBuilder sb = new StringBuilder(14 * getSize());

        for (Coordinate coordinate: coordinates){
            sb.append(coordinate.toShortString());
            sb.append(", ");
        }

        /*
        for (Area area: touches){
            touchesString += "\n  *" + area.toString();
        }*/

        return toString() + "\n\nCoordinates (X, Y): \n" + sb.toString();
    }
}
